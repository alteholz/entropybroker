Source: entropybroker
Section: utils
Priority: optional
Maintainer: Thorsten Alteholz <debian@alteholz.de>
Standards-Version: 4.7.0
Build-Depends: debhelper-compat (= 13)
        , libgd-dev
	, libasound2-dev
	, libusb-1.0-0-dev
	, libpng-dev
	, zlib1g-dev
	, libpcsclite-dev
	, libftdi-dev
	, libcrypto++-dev
	, cppcheck
	, dh-sequence-movetousr
Homepage: https://www.vanheusden.com/entropybroker/
Vcs-Browser: https://salsa.debian.org/alteholz/entropybroker
Vcs-Git: https://salsa.debian.org/alteholz/entropybroker.git
Rules-Requires-Root: no

Package: entropybroker
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: infrastructure for distributing random numbers (entropy data)
 Entropy Broker is an infrastructure for distributing cryptographically secure
 random numbers (entropy data) from one or more servers to one or more clients.
 .
 It allows you to distribute entropy data (random values) to /dev/random
 devices from other systems (real servers or virtualised systems).
 It helps preventing that the /dev/random device gets depleted; an empty
 /dev/random-device can cause programs to hang (waiting for entropy data to
 become available).
 .
 This is useful for systems that need to generate encryption keys, run VPN
 software or run a casino website. Also virtual systems that have no good
 sources of entropy like virtual servers (e.g. VMware, XEN and KVM (although
 KVM has the virtio_rnd driver)).
 .
 Entropy Broker is an infrastructure consisting of client-daemons that fill
 /dev/random and server-daemons that feed the central entropy broker-server.
 The server-daemons can gather random values by measuring timer frequency
 noise, analysing noise from a unused audio-device, noise from a video source
 (webcam, tv-card) and random values from a real hardware RNG (random number
 generator).
